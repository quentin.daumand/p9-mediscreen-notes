package com.mediscreen.notes.webclient;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest
@RunWith(SpringRunner.class)
public class PatientWebClientTest {
    @Autowired
    private PatientWebClient patientWebClient;

    @Test
    public void getListPatients() throws Exception {
        Assertions.assertThat(patientWebClient.getListPatients())
                .isNotNull()
                .isNotEmpty();
    }

    @Test
    public void getCheckPatientID() throws Exception {
        Assertions.assertThat(patientWebClient.checkPatientIdExist(1))
                .isNotNull();
    }

}

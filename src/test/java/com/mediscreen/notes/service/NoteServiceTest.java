package com.mediscreen.notes.service;

import com.mediscreen.notes.model.NoteModel;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class NoteServiceTest {
    @Autowired
    private NoteService noteService;

    public NoteModel note1() {
        LocalDateTime date = LocalDateTime.of(2021,11, 01, 8, 30,30);

        NoteModel noteModel1 = new NoteModel();
        noteModel1.setId("1245567");
        noteModel1.setPatId(1);
        noteModel1.setCreationDateTime(date);
        noteModel1.setE("Diabete Type A");
        return noteModel1;
    }

    public NoteModel note2() {
        LocalDateTime date = LocalDateTime.of(2021,07,02, 8,15,30);

        NoteModel noteModel2 = new NoteModel();
        noteModel2.setId("1245568");
        noteModel2.setPatId(2);
        noteModel2.setCreationDateTime(date);
        noteModel2.setE("Diabete Type B");
        return noteModel2;
    }

    @Before
    public void saveNotesToDbBeforeTests() {
        noteService.save(note1());
        noteService.save(note2());
    }

    @After
    public void deleteAllNotesAfterTests() {
        noteService.deleteAllNotes();
    }

    @Test
    public void getAllNotesByPatId() {
        List<NoteModel> listNotes = noteService.getAllNotesByPatId(1);
        Assert.assertTrue(listNotes.size() == 1);
        Assert.assertTrue(listNotes.get(0).getE().equals("Diabete Type A"));
    }

    @Test
    public void checkIdExists() {
        Assert.assertTrue(noteService.checkIdExists("1245568"));
    }

    @Test
    public void deleteNote() {
        noteService.deleteNote("1245568");
        Assert.assertFalse(noteService.checkIdExists("1245568"));
    }
}

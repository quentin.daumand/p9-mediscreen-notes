package com.mediscreen.notes.controller;

import com.mediscreen.notes.model.PatientModel;
import com.mediscreen.notes.webclient.PatientWebClient;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
public class PatientControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webContext;

    @MockBean
    PatientWebClient patientWebClient;

    @Before
    public void setupMockmvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();
    }

    public PatientModel Sophie() {
        LocalDate date = new LocalDate(1994, 07, 10);
        PatientModel Sophie = new PatientModel();
        Sophie.setGiven("Sophie");
        Sophie.setFamily("Princess");
        Sophie.setDob(date.toString());
        Sophie.setSex("F");
        Sophie.setAddress("1 rue du Paradis");
        Sophie.setEmailAddress("princessSophie@mail.fr");
        Sophie.setPhone("0102030405");
        return Sophie;
    }

    public PatientModel Luc() {
        LocalDate date = new LocalDate(1984, 9,04);
        PatientModel patientModel2 = new PatientModel();
        patientModel2.setGiven("Luc");
        patientModel2.setFamily("Besson");
        patientModel2.setDob(date.toString());
        patientModel2.setSex("M");
        patientModel2.setAddress("666 Rue de L'enfer");
        patientModel2.setEmailAddress("lub.besson@gmail.com");
        patientModel2.setPhone("001122334455");
        return patientModel2;
    }

    @Test
    public void getPatients() throws Exception {
        List<PatientModel> patientList = new ArrayList<>();
        patientList.add(Sophie());
        patientList.add(Luc());

        doReturn(patientList)
                .when(patientWebClient)
                .getListPatients();

        mockMvc.perform(get("/patients"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("patients"))
                .andExpect(model().attributeExists("patients"))
                .andReturn();

        Assert.assertTrue(patientList.size() == 2);
        Assert.assertTrue(patientList.get(0).getGiven().equals("Sophie"));
        Assert.assertTrue(patientList.get(0).getAddress().equals("1 rue du Paradis"));
        Assert.assertTrue(patientList.get(1).getGiven().equals("Luc"));
        Assert.assertTrue(patientList.get(1).getEmailAddress().equals("lub.besson@gmail.com"));
        Assert.assertTrue(patientList.get(1).getPhone().equals("001122334455"));
    }
}

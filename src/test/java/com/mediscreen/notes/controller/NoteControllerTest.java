package com.mediscreen.notes.controller;

import com.mediscreen.notes.model.NoteModel;
import com.mediscreen.notes.service.NoteService;
import com.mediscreen.notes.webclient.PatientWebClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
public class NoteControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webContext;

    @MockBean
    private NoteService noteService;

    @MockBean
    private PatientWebClient patientWebClient;

    @Before
    public void setupMockmvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();
    }


    public NoteModel note1() {
        LocalDateTime date = LocalDateTime.of(2021,11, 01, 8, 30,30);

        NoteModel noteModel1 = new NoteModel();
        noteModel1.setId("1245567");
        noteModel1.setPatId(1);
        noteModel1.setCreationDateTime(date);
        noteModel1.setE("Diabete Type A");
        return noteModel1;
    }

    public NoteModel note2() {
        LocalDateTime date = LocalDateTime.of(2021,07,02, 8,15,30);

        NoteModel noteModel2 = new NoteModel();
        noteModel2.setId("1245568");
        noteModel2.setPatId(2);
        noteModel2.setCreationDateTime(date);
        noteModel2.setE("Diabete Type B");
        return noteModel2;
    }

    @Test
    public void getPatHistoryOK() throws Exception {
        List<NoteModel> notesList = new ArrayList<>();
        notesList.add(note1());
        notesList.add(note2());

        doReturn(true)
                .when(patientWebClient)
                .checkPatientIdExist(1);

        doReturn(notesList)
                .when(noteService)
                .getAllNotesByPatId(1);

        mockMvc.perform(get("/patHistory/{patId}", "1"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("history"))
                .andExpect(model().attributeExists("notes"))
                .andReturn();

        assertTrue(notesList.get(0).getE().equals("Diabete Type A"));
        assertTrue(notesList.get(1).getE().equals("Diabete Type B"));
    }


    @Test
    public void getPatHistoryKO() throws Exception {
        List<NoteModel> notesList = new ArrayList<>();
        notesList.add(note1());
        notesList.add(note2());

        doReturn(false)
                .when(patientWebClient)
                .checkPatientIdExist(1);

        doReturn(notesList)
                .when(noteService)
                .getAllNotesByPatId(1);

        mockMvc.perform(get("/patHistory/{patId}", "1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andReturn();

    }

    @Test
    public void patHistoryAdd() throws Exception {
        doReturn(true)
                .when(patientWebClient)
                .checkPatientIdExist(1);

        mockMvc.perform(get("/patHistory/add/{patId}", "1"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("add"))
                .andReturn();
    }

    @Test
    public void patHistoryAddKO() throws Exception {
        doReturn(false)
                .when(patientWebClient)
                .checkPatientIdExist(1);

        mockMvc.perform(get("/patHistory/add/{patId}", "1")
                        .flashAttr("ErrorPatientId", "Patient ID doesn't exist"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andExpect(flash().attributeExists("ErrorPatientId"))
                .andReturn();
    }

    @Test
    public void postNotesOK() throws Exception {
        note1().setCreationDateTime(LocalDateTime.now());

        doReturn(true)
                .when(patientWebClient)
                .checkPatientIdExist(1);

        doReturn(note1())
                .when(noteService)
                .save(note1());

        mockMvc.perform(post("/patHistory/add")
                        .flashAttr("successSave", "Your patient was successfully saved")
                        .param("id", "1")
                        .param("patId", "1")
                        .param("creationDateTime", "2021-02-22T13:45")
                        .param("e", "Diabete Type A"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patHistory/1"))
                .andExpect(flash().attributeExists("successSave"))
                .andReturn();
    }

    @Test
    public void postNotesOKBis() throws Exception {
        note1().setCreationDateTime(LocalDateTime.now());

        doReturn(true)
                .when(patientWebClient)
                .checkPatientIdExist(1);

        doReturn(true)
                .when(noteService)
                .checkIdExists("1");

        doReturn(note1())
                .when(noteService)
                .update(note1());

        mockMvc.perform(post("/patHistory/add")
                        .flashAttr("successSave", "Your patient was successfully saved")
                        .param("id", "1")
                        .param("patId", "1")
                        .param("creationDateTime", "2021-02-22T13:45")
                        .param("e", "Diabete Type A"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patHistory/1"))
                .andExpect(flash().attributeExists("successSave"))
                .andReturn();
    }

    @Test
    public void postPatHistoryKO() throws Exception {
        note1().setCreationDateTime(LocalDateTime.now());

        doReturn(false)
                .when(patientWebClient)
                .checkPatientIdExist(1);

        mockMvc.perform(post("/patHistory/add")
                        .flashAttr("successSave", "Your patient was successfully saved")
                        .param("id", "1")
                        .param("patId", "1")
                        .param("creationDateTime", "2021-02-22T13:45")
                        .param("e", "Diabete Type A"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:patients"))
                .andExpect(flash().attributeExists("ErrorPatientId"))
                .andReturn();
    }

    @Test
    public void postPatHistoryKOBis() throws Exception {
        note1().setE("");

        doReturn(true)
                .when(patientWebClient)
                .checkPatientIdExist(1);

        doReturn(note1())
                .when(noteService)
                .save(note1());

        mockMvc.perform(post("/patHistory/add")
                        .flashAttr("successSave", "Your patient was successfully saved")
                        .param("id", "1")
                        .param("patId", "1")
                        .param("creationDateTime", "2021-02-22T13:45")
                        .param("e", ""))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patHistory/add/1"))
                .andReturn();
    }

    @Test
    public void deleteNotesOK() throws Exception {
        doReturn(true)
                .when(noteService)
                .checkIdExists("1245567");

        doNothing()
                .when(noteService)
                .deleteNote("1245567");

        mockMvc.perform(delete("/patHistory/{id}", "1245567")
                        .flashAttr("successDelete", "This note was successfully deleted"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andExpect(flash().attributeExists("successDelete"))
                .andReturn();
    }

    @Test
    public void deleteNotesKO() throws Exception {
        doReturn(true)
                .when(noteService)
                .checkIdExists("1245567");

        mockMvc.perform(delete("/patHistory/{id}", "12467")
                        .flashAttr("ErrorNoteId", "ID doesn't exist"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patHistory/{id}"))
                .andExpect(flash().attributeExists("ErrorNoteId"))
                .andReturn();
    }

    @Test
    public void patHistoryUpdateOK() throws Exception {
        doReturn(true)
                .when(noteService)
                .checkIdExists("1");

        doReturn(note1())
                .when(noteService)
                .getNoteById("1");

        mockMvc.perform(get("/patHistory/update/{patId}", "1"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("add"))
                .andReturn();
    }

    @Test
    public void patHistoryUpdateKO() throws Exception {
        doReturn(false)
                .when(noteService)
                .checkIdExists("1");

        mockMvc.perform(get("/patHistory/update/{patId}", "1")
                        .flashAttr("ErrorPatientId", "Patient ID doesn't exist"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patHistory/{id}"))
                .andExpect(flash().attributeExists("ErrorNoteId"))
                .andReturn();
    }


}

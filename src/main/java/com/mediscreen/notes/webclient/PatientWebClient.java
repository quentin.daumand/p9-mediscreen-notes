package com.mediscreen.notes.webclient;

import com.mediscreen.notes.model.PatientModel;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class PatientWebClient {

   private final String BASE_URL_LOCALHOST = "http://localhost:8081";
    //for DOCKER
    //private final String BASE_URL_LOCALHOST = "http://patients:8081";
    // Declare the path for patient list
    private final String PATH_PATIENTS_JSON = "/patients/json";
    // Declare the path for checking a patient ID
    private final String PATH_PATIENT_EXIST = "/check/patient/";

    /**
     * Web Client request to server-service "patients" for getting a list of all patients
     *
     * @return PatientModel list of all patients
     */
    public List<PatientModel> getListPatients() {
        Flux<PatientModel> getPatientList= WebClient.create()
                .get()
                .uri( BASE_URL_LOCALHOST + PATH_PATIENTS_JSON)
                .retrieve()
                .bodyToFlux(PatientModel.class);
        List<PatientModel> patientList = getPatientList.collectList().block();
        return patientList;
    }

    public boolean checkPatientIdExist(int patientId) {
        Mono<Boolean> getPatientList= WebClient.create()
                .get()
                .uri(BASE_URL_LOCALHOST + PATH_PATIENT_EXIST + patientId)
                .retrieve()
                .bodyToMono(Boolean.class);
        boolean patientExists = getPatientList.block();
        return patientExists;
    }
}

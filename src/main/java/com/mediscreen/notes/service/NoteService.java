package com.mediscreen.notes.service;

import com.mediscreen.notes.model.NoteModel;
import com.mediscreen.notes.repository.NoteRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {

    private final NoteRepository noteRep;

    public NoteService(NoteRepository noteRep) {
        this.noteRep = noteRep;
    }

    public List<NoteModel> getAllNotesByPatId(int patId) {
        return noteRep.findAllByPatId(patId);
    }

    public NoteModel save(NoteModel note) {
        return noteRep.save(note);
    }

    public void deleteAllNotes() {
        noteRep.deleteAll();
    }

    public boolean checkIdExists(String id) {
        return noteRep.existsById(id);
    }

    public void deleteNote(String id) {
        noteRep.deleteById(id);
    }

    public NoteModel getNoteById(String id) {
        return noteRep.getById(id);
    }

    public NoteModel update(NoteModel note) {
        return noteRep.save(note);
    }
}

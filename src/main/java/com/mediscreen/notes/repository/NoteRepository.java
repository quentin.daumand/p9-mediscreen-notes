package com.mediscreen.notes.repository;

import com.mediscreen.notes.model.NoteModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface NoteRepository extends MongoRepository<NoteModel, Integer> {

    List<NoteModel> findAllByPatId(int patId);

    boolean existsById(String id);

    void deleteById(String id);

    NoteModel getById(String id);
}
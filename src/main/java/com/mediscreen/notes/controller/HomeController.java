package com.mediscreen.notes.controller;

import com.mediscreen.notes.webclient.PatientWebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    private final Logger LOGGER = LoggerFactory.getLogger(PatientController.class);

    @Autowired
    PatientWebClient patientWebClientService;

    @GetMapping("/")
    public String index(Model model) {
        LOGGER.info("GET /home : OK");
        return "home";
    }
}

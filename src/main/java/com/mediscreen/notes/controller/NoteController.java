package com.mediscreen.notes.controller;

import com.mediscreen.notes.model.NoteModel;
import com.mediscreen.notes.service.NoteService;
import com.mediscreen.notes.webclient.PatientWebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class NoteController {
    private final Logger LOGGER = LoggerFactory.getLogger(NoteController.class);

    @Autowired
    NoteService noteService;

    @Autowired
    PatientWebClient patientWebClient;

    @GetMapping("/patHistory/{patId}")
    public String patHistory(@PathVariable("patId") int patId, Model model, RedirectAttributes ra) {
        if (!patientWebClient.checkPatientIdExist(patId)) {
            ra.addFlashAttribute("ErrorPatientId", "Patient ID doesn't exist");
            LOGGER.info("GET patHistory : Patient ID Not Exists");
            return "redirect:/patients";
        }
        model.addAttribute("notes", noteService.getAllNotesByPatId(patId));
        model.addAttribute("notePatId", patId);
        LOGGER.info("GET /patHistory/ : OK");
        return "history";
    }

    @GetMapping("/patHistory/add/{patId}")
    public String patHistoryAdd(@PathVariable("patId") int patId, final Model model, RedirectAttributes ra) {
        if (!patientWebClient.checkPatientIdExist(patId)) {
            ra.addFlashAttribute("ErrorPatientId", "Patient ID doesn't exist");
            LOGGER.info("GET patHistory : Patient ID Not Exists");
            return "redirect:/patients";
        }
        NoteModel newNoteModel = new NoteModel();
        newNoteModel.setPatId(patId);
        if(!model.containsAttribute("note")) {
            model.addAttribute("note", newNoteModel);
        }
        LOGGER.info("GET /patHistory/add : OK");
        return "add";
    }

    @PostMapping("/patHistory/add")
    public String postNoteAdd(@Valid @ModelAttribute("note") final NoteModel note,
                              final BindingResult result, final RedirectAttributes redirectAttributes) {
        if (!patientWebClient.checkPatientIdExist(note.getPatId())) {
            redirectAttributes.addFlashAttribute("ErrorPatientId", "Patient ID doesn't exist");
            LOGGER.info("GET /patHistory/add : Non existent id");
            return "redirect:patients";
        }
        if (!result.hasErrors()) {
            note.setCreationDateTime(LocalDateTime.now());
            if (noteService.checkIdExists(note.getId())) {
                noteService.update(note);
                LOGGER.info("POST /patHistory/add : OK UPDATE");
            } else {
                noteService.save(note);
                LOGGER.info("POST /patHistory/add : OK SAVE");
            }
            redirectAttributes.addFlashAttribute("successSave", "Your note was successfully added");
            return "redirect:/patHistory/"+note.getPatId();
        }
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.note", result);
            redirectAttributes.addFlashAttribute("note", note);
            LOGGER.info("POST /patHistory/add : KO");
        }
        return "redirect:/patHistory/add/"+note.getPatId();
    }

    @DeleteMapping("/patHistory/{id}")
    public String noteDelete(@PathVariable("id") String id, RedirectAttributes ra) {
        if (!noteService.checkIdExists(id)) {
            ra.addFlashAttribute("ErrorNoteId", "ID doesn't exist");
            LOGGER.info("DELETE /patHistory/ : Id Note Not Exists");
            return "redirect:/patHistory/{id}";
        }
        noteService.deleteNote(id);
        ra.addFlashAttribute("successDelete", "This note was successfully deleted");
        LOGGER.info("DELETE /patHistory : OK");

        return "redirect:/patients";
    }

    @GetMapping("/patHistory/update/{id}")
    public String patientUpdate(@PathVariable("id") String id, Model model, RedirectAttributes ra) {
        if (!noteService.checkIdExists(id)) {
            ra.addFlashAttribute("ErrorNoteId", "ID doesn't exist");
            LOGGER.info("GET /patHistory/update : Id Note Not Exists");
            return "redirect:/patHistory/{id}";
        }

        model.addAttribute("note", noteService.getNoteById(id));
        LOGGER.info("GET /patHistory/update : OK");
        return "add";
    }

    @GetMapping("/patHistory/list")
    public @ResponseBody
    List<NoteModel> getNoteList(Integer patId) {
        LOGGER.info("GET /getNoteList : OK");
        return noteService.getAllNotesByPatId(patId);
    }

}

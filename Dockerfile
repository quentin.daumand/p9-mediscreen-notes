FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /usr/app
COPY build/libs/p9-mediscreen-notes-1.0-SNAPSHOT.jar mediscreen-notes.jar
EXPOSE 8082
CMD ["java", "-jar", "mediscreen-notes.jar"]
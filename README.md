<h1>Readme</h1>
<h2># P9 MEDISCREEN PATIENTS</h2>
<h3>## Purpose</h3>
<p> Mediscreen specializes in detecting risk factors for disease. Our screenings are using predictive analysis of patient populations at an affordable cost
    This application is designed to:
<ul>
    <li>to handle a MySQL Database</li>
    <li>handle patient's notes in a Mongo DB</li>
    <li>provide a screening of all patients according to specific criteria (see ENUM classes)</li>
</ul>
</p>

<h3>## Prerequisites to run</h3>
<ul>
    <li>Java JDK11</li>
    <li>Gradle 4.8.1</li>
    <li>Docker</li>
    <li>MongoDB Compass & an account</li>
    <li>MySQL 8.0</li>
</ul>

<h3>## Installing</h3>
<p>
<ol>
    <li>Install <strong>Java</strong>: https://www.oracle.com/java/technologies/javase-downloads.html</li>
    <li>Install <strong>Gradle</strong>: https://gradle.org/install/</li>
    <li>Install <strong>Docker Desktop</strong>: https://docs.docker.com/docker-for-windows/</li>
    <li>Install <strong>MySQL</strong>: https://dev.mysql.com/downloads/mysql/</li>
    <li>Install <strong>MongoDB Compass</strong>: https://www.mongodb.com/products/compass</li>
</ol>
</p>

<h3>## Technical Specifications</h3>
<p>
    Mediscreen is composed of multiple microservices:
<ol>
    <li>Patients: https://gitlab.com/quentin.daumand/p9-mediscreen-patients</li>
    <li>Notes: https://gitlab.com/quentin.daumand/p9-mediscreen-notes</li>
    <li>Risk: https://gitlab.com/quentin.daumand/p9-mediscreen-risk</li>
</ol>
</p>

<h3>## Run the application</h3>
<p>
    <strong>WITH IDE/GRADLE</strong>: Use the BASE_URL_LOCALHOST constant present in files PatientWebClientService/RecordWebClientService in microservice risk :src/main/java/com/mediscreen/risk/service/webclient and in
    in microservice notes :src/main/java/com/mediscreen/notes/service/

    For running the application, either launch it in your IDE or run below command inside the root directory of the 3 microservices.
    You will also need to change the mongoDB URL in application.properties of microservice notes, to work with your cluster.
<pre>
    $ ./gradlew bootRun
    </pre>

<strong>WITH DOCKER</strong>Use this exact procedure:
<ol>
    <li>Build your application with gradle <pre>$ ./gradlew build</pre></li>
    <li>Build docker images for each microservice. In each microservice root directory, launch the following command:<pre>$ docker build -t NAME_OF_YOUR_IMAGE:TAGVERSION .</pre></pre></li>
    <li>Go into root directory of microservice risk and launch this command, which is going to launch and compose the Docker containers from images previously created, but also run them.
        You will then deploy with Docker all the applications running with Spring Boot.</li>
</ol>

<strong>Tips</strong>Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application's services.

Then, with a single command, you create and start all the services from your configuration. ...
Run docker-compose up and Compose starts and runs your entire app.

<pre>$ docker-compose up</pre>
</p>

<h3>## Endpoints</h3>
<pre>
    > **GET** - README
    http://localhost:8082/
    
    > **GET** - get History for a patient patId
    http://localhost:8082/patHistory/{patId}
    
    > **GET** - add notes for a patient - view
    http://localhost:8082/patHistory/add/{patId}
    
    > **POST** - add notes for a patient
    http://localhost:8082/patHistory/add/
    
    > **DELETE** - delete History with id
    http://localhost:8082/patHistory/{id}  
    
    > **GET** - update History with id
    http://localhost:8082/update/{id}
    
    > **GET** - get list of notes for one patient
    http://localhost:8082/patHistory/list
</pre>

<h3>## Testing</h3>
<p>
    Mediscreen has a full integration and unit test suite in each microservice. You can launch it with the following command inside the root directory:

<pre>
        $ ./gradlew test
    </pre>

Or for running the tests inside your IDE, follow the link below:
[Work with tests in Gradle](https://www.jetbrains.com/help/idea/work-with-tests-in-gradle.html#configure_gradle_test_runner).
</p>

<h3>## Reporting</h3>
<p>
    JaCoCo reporting tools are attached to the gradle configuration. Launch these 2 commands to run the tools:
<pre>
    $ ./gradlew jacocoTestReport
    $ ./gradlew jacocoTestCoverageVerification
    </pre>
</p>